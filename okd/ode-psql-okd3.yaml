apiVersion: v1
kind: Template
metadata:
  name: ode-psql
  annotations:
    openshift.io/display-name: "Odoo + Postgres"
    description: Ung dung odoo chay tren openshift (okd)
    iconClass: icon-python
    openshift.io/documentation-url: "https://gitlab.com/nuttim/odoo-okd"
    openshift.io/provider-display-name: "NUTTIM"
objects:
- apiVersion: v1
  kind: Secret
  metadata:
    name: '${NAME}'
  stringData:
    database-password: '${DATABASE_PASSWORD}'
    database-user: '${DATABASE_USER}'
    secret-key: '${ODE_SECRET_KEY}'
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: 'pvc-${NAME}'
  spec:
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: '${VOLUME_CAPACITY}'
- apiVersion: v1
  kind: Service
  metadata:
    name: '${NAME}'
  spec:
    ports:
      - name: '${NAME}-port'
        port: 8080
        targetPort: 8069
    selector:
      name: '${NAME}'
- apiVersion: v1
  kind: Route
  metadata:
    name: '${NAME}'
  spec:
    host: '${APPLICATION_DOMAIN}'
    to:
      kind: Service
      name: '${NAME}'
- apiVersion: v1
  kind: ImageStream
  metadata:
    name: '${NAME}'
- apiVersion: v1
  kind: BuildConfig
  metadata:
    name: '${NAME}'
  spec:
    output:
      to:
        kind: ImageStreamTag
        name: '${NAME}:latest'
    source:
      git:
        ref: '${SOURCE_REPOSITORY_REF}'
        uri: '${SOURCE_REPOSITORY_URL}'
      type: Git
    strategy:
      dockerStrategy:
        dockerfilePath: '${SOURCE_DOCKERFILE}'
        env: 
    triggers:
      - type: ImageChange
      - type: ConfigChange
      - github:
          secret: '${GITHUB_WEBHOOK_SECRET}'
        type: GitHub
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: '${NAME}'
  spec:
    replicas: 1
    selector:
      name: '${NAME}'
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: '${NAME}'
        name: '${NAME}'
      spec:
        containers:
          - env:
              - name: HOST
                value: '${DATABASE_SERVICE_NAME}'
              - name: USER
                valueFrom:
                  secretKeyRef:
                    key: database-user
                    name: '${NAME}'
              - name: PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: database-password
                    name: '${NAME}'
              - name: DATABASE_NAME
                value: '${DATABASE_NAME}'
              - name: APP_SECRET_KEY
                valueFrom:
                  secretKeyRef:
                    key: secret-key
                    name: '${NAME}'
            image: ' '
            name: '${NAME}'
            ports:
              - containerPort: 8080
            resources:
              limits:
                memory: '${MEMORY_LIMIT}'
            volumeMounts:
            - mountPath: /var/lib/odoo
              name: '${NAME}-vol1'
            - mountPath: /mnt/extra-addons
              name: '${NAME}-vol2'
        volumes:
        - name: '${NAME}-vol1'
        - name: '${NAME}-vol2'
          persistentVolumeClaim:
            claimName: pvc-${NAME}
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - '${NAME}'
          from:
            kind: ImageStreamTag
            name: '${NAME}:latest'
        type: ImageChange
      - type: ConfigChange
- apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: 'pvc-${DATABASE_SERVICE_NAME}'
  spec:
    accessModes:
      - ReadWriteOnce
    resources:
      requests:
        storage: '${VOLUME_CAPACITY}'
- apiVersion: v1
  kind: Service
  metadata:
    name: '${DATABASE_SERVICE_NAME}'
  spec:
    ports:
      - name: '${DATABASE_SERVICE_NAME}-port'
        port: 5432
        targetPort: 5432
    selector:
      name: '${DATABASE_SERVICE_NAME}'
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    name: '${DATABASE_SERVICE_NAME}'
  spec:
    replicas: 1
    selector:
      name: '${DATABASE_SERVICE_NAME}'
    strategy:
      type: Recreate
    template:
      metadata:
        labels:
          name: '${DATABASE_SERVICE_NAME}'
        name: '${DATABASE_SERVICE_NAME}'
      spec:
        containers:
          - env:
              - name: POSTGRESQL_USER
                valueFrom:
                  secretKeyRef:
                    key: database-user
                    name: '${NAME}'
              - name: POSTGRESQL_PASSWORD
                valueFrom:
                  secretKeyRef:
                    key: database-password
                    name: '${NAME}'
              - name: POSTGRESQL_DATABASE
                value: '${DATABASE_NAME}'
            image: ' '
            livenessProbe:
              exec:
                command:
                  - /usr/libexec/check-container
                  - '--live'
              initialDelaySeconds: 120
              timeoutSeconds: 10
            name: '${DATABASE_SERVICE_NAME}'
            ports:
              - containerPort: 5432
            readinessProbe:
              exec:
                command:
                  - /usr/libexec/check-container
              initialDelaySeconds: 5
              timeoutSeconds: 1
            resources:
              limits:
                memory: '${MEMORY_POSTGRESQL_LIMIT}'
            volumeMounts:
            - mountPath: /var/lib/pgsql/data
              name: '${DATABASE_SERVICE_NAME}-vol1'
        volumes:
        - name: '${DATABASE_SERVICE_NAME}-vol1'
          persistentVolumeClaim:
            claimName: 'pvc-${DATABASE_SERVICE_NAME}'
    triggers:
      - imageChangeParams:
          automatic: true
          containerNames:
            - '${DATABASE_SERVICE_NAME}'
          from:
            kind: ImageStreamTag
            name: 'postgresql:${POSTGRESQL_VERSION}'
            namespace: '${NAMESPACE}'
        type: ImageChange
      - type: ConfigChange
parameters:
- name: NAME
  displayName: Name
  description: >-
    The name assigned to all of the frontend objects defined in this
    template.
  value: web
  required: true
- name: NAMESPACE
  displayName: Namespace
  description: The OpenShift Namespace where the ImageStream resides.
  value: openshift
  required: true
- name: POSTGRESQL_VERSION
  displayName: Version of PostgreSQL Image
  description: Version of PostgreSQL image to be used (10 or latest).
  value: '10'
  required: true
- name: MEMORY_LIMIT
  displayName: Memory Limit
  description: Maximum amount of memory the container can use.
  value: 512Mi
  required: true
- name: MEMORY_POSTGRESQL_LIMIT
  displayName: Memory Limit (PostgreSQL)
  description: Maximum amount of memory the PostgreSQL container can use.
  value: 512Mi
  required: true
- name: VOLUME_CAPACITY
  displayName: Volume Capacity
  description: 'Volume space available for data, e.g. 512Mi, 2Gi'
  value: 1Gi
  required: true
- name: SOURCE_REPOSITORY_URL
  displayName: Git Repository URL
  description: The URL of the repository with your application source code.
  value: 'https://gitlab.com/nuttim/odoo-okd.git'
  required: true
- name: SOURCE_REPOSITORY_REF
  displayName: Git Reference
  description: >-
    Set this to a branch name, tag or other ref of your repository if you
    are not using the default branch.
- name: SOURCE_DOCKERFILE
  displayName: Soure docker file. Dockerfile.max, Dockerfile.min, Dockerfile.13,.... 
  description: 
  value: 'Dockerfile.min'
  required: true
- name: APPLICATION_DOMAIN
  displayName: Application Hostname
  description: >-
    The exposed hostname that will route to the service, if left
    blank a value will be defaulted.
- name: GITHUB_WEBHOOK_SECRET
  displayName: GitHub Webhook Secret
  description: >-
    Github trigger secret.  A difficult to guess string encoded as part of
    the webhook URL.  Not encrypted.
  generate: expression
  from: '[a-zA-Z0-9]{40}'
- name: DATABASE_SERVICE_NAME
  displayName: Database Service Name
  value: db
  required: true
- name: DATABASE_NAME
  displayName: Database Name
  value: odoo
  required: true
- name: DATABASE_USER
  displayName: Database Username
  value: odoo
  required: true
- name: DATABASE_PASSWORD
  displayName: Database User Password
  generate: expression
  from: '[a-zA-Z0-9]{16}'
- name: ODE_SECRET_KEY
  displayName: Secret Key
  description: Set this to a long random string.
  generate: expression
  from: '[\w]{50}'
labels:
app: 'ode-psql'
template: 'ode-psql'
