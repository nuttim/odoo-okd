# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'MRP: Subcontracing',
    'category': 'MRP',
    'description': """
Sub Contracting of manufacturing work orders
=====================================

    """,
    'depends': ['mrp','mrp_workorder'],
    'data': [
        'views/mrp_subcontracting_views.xml',
        'security/ir.model.access.csv',
    ],
}
