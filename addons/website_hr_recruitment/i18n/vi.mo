��    ^           �      �     �     �            I     \   i  G   �  I   	  C   X	  N   �	  F   �	  G   2
  >   z
  I   �
  :     3   >  �   r     o     �  !   �  )   �     �     
          (  	   4     >  	   J  
   T     _  k   n     �     �     �  
          L   0  
   }  P   �     �  i   �  $   T     y     �     �     �     �  
   �     �     �  	   �     �  /   �  �   (  H   �     /     K  E   [  U   �     �       C   #     g     {  6   �  6   �  
   �                 ?  (     h     }     �     �     �  K   �               ,     L     d     }     �  R   �  
   �  	   	       .   %  "   T     w     �     �  �  �     =     B     S     \  I   d  \   �  G     D   S  0   �  F   �  3     >   D  1   �  >   �  %   �  4     �   O     �  !        -  5   M  (   �     �     �     �     �     �          .     A  �   P  $   �  ,        3  
   A  -   L  c   z  
   �  V   �      @  �   a  ;         C      c      o   &   �      �      �      �      �      !     !!  P   0!  �   �!  Y   ,"  2   �"     �"  r   �"  X   K#  &   �#  %   �#  ]   �#  %   O$     u$  ]   �$  ;   �$  
   $%     /%     J%     _%  P  u%     �&     �&      �&     '  (   '  d   C'     �'     �'  C   �'     (  )   4(     ^(  )   o(  |   �(     )     )     !)  B   6)  /   y)     �)     �)     �)               	       6   D      .          ?      9   8          U       J   N   >   7          A   &       =       B       3              X   ]       '   #          /   \          (   M   R      P       ,   4   W               Y      1   0   !       "   G   ^   ;         V   H      I                         5   K   +       @       Q       *       Z      :   %   F   )       C   <      T                 $   E      S           O      L      -          2         
          [    $15k 10  / 40 people 3 months 50% YoY <br/><i class="fa fa-phone" role="img" aria-label="Phone" title="Phone"/> <i class="fa fa-clock-o" title="Publication date" role="img" aria-label="Publication date"/> <i class="fa fa-envelope" role="img" aria-label="Email" title="Email"/> <span class="fa fa-2x fa-calendar"/>
                    Sponsored Events <span class="fa fa-2x fa-car"/>
                    Save on commute <span class="fa fa-2x fa-check-circle"/>
                    Discount Programs <span class="fa fa-2x fa-coffee"/>
                    Eat &amp; Drink <span class="fa fa-2x fa-futbol-o"/>
                    Sport Activity <span class="fa fa-2x fa-heart"/>
                    Benefits <span class="fa fa-2x fa-map-marker"/>
                    Prime location <span class="fa fa-2x fa-sun-o"/>
                    PTOs <span class="o_recruitment_purple">Published</span> <span>
                                            We usually reply between one and three days.<br/>
                                            Feel free to contact him/her if you have further questions.
                                        </span> <strong>Challenges</strong> <strong>Must Have</strong> <strong>Responsibilities</strong> <strong>What's great in the job?</strong> Achieve monthly targets All Countries All Departments All Offices Applicant Applied Job Apply Job Apply Now! Avg Deal Size: Brand-name product and services in categories like travel, electronics, health, fitness, cellular, and more Company Growth: Company Maturity: Congratulations! Contact us Continue To Our Website Create new job pages from the <strong><i>+New</i></strong> top-right button. Department Direct coordination with functional consultants for qualification and follow ups Full sales cycle Healthcare, dental, vision, life insurance, Flexible Spending Account (FSA), Health Savings Account (HSA) High commissions for good performers In the meantime, Job Job Application Form Job Complexity: Job Description Job Detail Job Position Job Security: Job Title Jobs Join us and help disrupt the enterprise market! Join us, we offer you an extraordinary chance to learn, to
                                    develop and to be part of an exciting experience and
                                    team. Large apps scope: CRM, MRP, Accounting, Inventory, HR, Project Mgt, etc. Look around on our website: Need More Info? No outbound calls, you get leads and focus on providing value to them Only a couple blocs from BART, Caltrain, Highway 101, carpool pickup, and Bay Bridge. Our Job Offers Overachieving Possibilities: Peet's and Philz coffee provided all day to order and pantry snacks Personal Evolution: Phone Number Play any sport with colleagues and the bill is covered Pre-tax commuter benefitsbr <br/>(parking and transit) Profitable Resume Sales Cycle: Short Introduction Short summary of the job: A sales job for smart people and can
                learn quickly new industries management practices. You will be
                in charge of the full sales cycle from the opportunity
                qualification to the negotiation, going through astonishing
                product demos. Source of Applicants Submit Team / Company Size: The culture The founder’s story Tuesday Dinners, Monthly Lunch Mixers, Monthly Happy Hour, Annual day event US + Canada Territory Url Parameters Vacation, Sick, and paid leaves Variability of the Job: Website Recruitment Form Website description What people say about us? You sell management software to directors of SMEs: interesting projects and people Your Email Your Name Your Phone Number Your application has been posted successfully. Your application has been sent to: for job opportunities. open positions unpublished Project-Id-Version: Odoo Server saas~12.5
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-06-08 14:37+0700
Last-Translator: Dao Nguyen <trucdao.uel@gmail.com>, 2019
Language-Team: Vietnamese (https://www.transifex.com/odoo/teams/41243/vi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
X-Generator: Poedit 2.3.1
 $15k 10 / 40 người 3 tháng 50% YoY <br/><i class="fa fa-phone" role="img" aria-label="Phone" title="Phone"/> <i class="fa fa-clock-o" title="Publication date" role="img" aria-label="Publication date"/> <i class="fa fa-envelope" role="img" aria-label="Email" title="Email"/> <span class="fa fa-2x fa-calendar"/>
Sự kiện được tài trợ <span class="fa fa-2x fa-car"/>
Lưu thay đổi <span class="fa fa-2x fa-check-circle"/>
Chương trình khuyến mãi <span class="fa fa-2x fa-coffee"/>
Ăn &amp; Uống <span class="fa fa-2x fa-futbol-o"/>
Hoạt động thể thao <span class="fa fa-2x fa-heart"/>
Các lợi ích <span class="fa fa-2x fa-map-marker"/>
Vị trí thuận lợi <span class="fa fa-2x fa-sun-o"/>
PTO <span class="o_recruitment_purple">Công bố</span> <span>
Chúng tôi sẽ trả lời bạn trong một đến ba ngày.
<br/>
Hãy liên hệ chúng tôi nếu bạn có bất kỳ câu hỏi nào.
</span> <strong>Thách thức</strong> <strong>Cần phải có</strong> <strong>Trách nhiệm</strong> <strong>Công việc này có gì thú vị?</strong> Mục tiêu mỗi tháng đạt được Tất cả quốc gia Tất cả phòng ban Tất cả nhân viên Ứng viên Vị trí ứng tuyển Ứng tuyển công việc Ứng tuyển ngay Avg Deal Size: Sản phẩm và dịch vụ trong thuộc các danh mục như du lịch, điện tử, sức khoẻ,  thể dục thể thao, di động, v.v. Sự tăng trưởng của công ty: Mức độ trưởng thành của công ty: Chúc mừng! Liên hệ Tiếp tục đến Website của chúng tôi Tạo mới các trang việc làm từ <strong><i>+Thêm mới</i></strong>nút trên góc phải. Phòng/Ban Liên kết trực tiếp đến các tư vấn viên để phân loại và theo dõi Toàn bộ chu trình bán hàng Chăm sóc sức khoẻ, nha khoa, thị lực, bảo hiểm nhân thọ, tài khoản chi tiêu linh hoạt (FSA), tài khoản tiết kiệm cho sức khoẻ (HSA) Hoa hồng cao cho nhân viên có năng lực xuất sắc Trong thời gian chờ đợi, Chức vụ Biểu mẫu ứng tuyển Độ phức tạp của công việc: Mô tả công việc Chi tiết công việc Chức vụ Tính an toàn công việc: Chức danh công việc Tuyển dụng Tham gia cùng chúng tôi và cùng khuấy đảo thị trường kinh doanh! Tham gia cùng chúng tôi, bạn sẽ có cơ hội tuyệt với để học hỏi, 
phát triển và trở thành một phần 
của một đội ngũ tuyệt vời. Các ứng dụng có quy mô lớn: CRM, MRP, Kế toán, Kho, Nhân sự, Dự án, v.v Dạo một vòng trên website của chúng tôi: Cần nhiều thông tin hơn? Không sử dụng telesales, bạn nhận các khách hàng tiềm năng và cung cấp các giá trị cho họ. Chỉ một vài khối từ BART, Caltrain, Highway 101, carpool pickup và Bay Brigde. Chúng tôi đang cần tuyển dụng Khả năng chịu đựng áp lực: Cà phê Peet's and Philz cung cấp dịch vụ đặt hàng và đồ ăn nhẹ trong ngày Sự phát triển của bản thân: Số Điện thoại Chơi thể thao nào với đồng nghiệp mà không còn phải quan tâm đến chi phí Hỗ trợ đi lại <br/> (bãi đậu xe và di chuyển) Lợi ích Lý lịch nghề nghiệp Chu kỳ bán hàng: Mô tả ngắn gọn Mô tả chung về công việc: Công việc bán hàng cho những bạn nhanh nhạy và thông minh, có thể học hỏi nhanh về các lĩnh vực mới.
Bạn sẽ phụ trách toàn bộ hoạt động trong chu kỳ bán hàng từ đánh giá
các cơ hội đến bước thương lượng và giới thiệu sản phẩm. Nguồn của ứng viên Gửi Quy mô nhóm / quy mô công ty Văn hoá công ty Câu chuyện của người sáng lập Bữa tối thứ ba, bữa trưa hàng tháng, happy hour hàng tháng, các sự kiện hàng năm Lãnh thổ Mỹ + Canada Thông số Url Ngày nghỉ phép, nghỉ bệnh, nghỉ lễ và nghỉ du lịch Sự thay đổi công việc Biểu mẫu tuyển dụng trên Website Mô tả Website Mọi người nói gì về chúng tôi? Bạn bán phần mềm quản lý cho các doanh nghiệp vừa và nhỏ: các dự án và những con người thú vị Email Tên Số điện thoại Đơn ứng tuyển của bạn đã được đăng thành công. Đơn ứng tuyển đã được gửi đến: cơ hội nghề nghiệp vị trí đang tuyển chưa xuất bản 